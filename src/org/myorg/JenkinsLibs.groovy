package org.myorg

def checkOutFrom(repo) {
//   git url: "git@github.com:jenkinsci/${repo}"
  git branch: "${params.BRANCH}", url: "https://gitlab.com/der-andrew/${repo}"
}

return this
